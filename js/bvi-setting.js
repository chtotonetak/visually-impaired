window.bvi = {
    bvi_setting: {
        BviPanel: 1,
        BviPanelBg: 'white',
        BviPanelFontSize: 12,
        BviPanelLetterSpacing: 'normal',
        BviPanelLineHeight: 'normal',
        BviPanelImg: 1,
        BviPanelImgXY: 1,
        BviPanelReload: 0,
        BviPanelNoWork: 0,
        BviPanelText: 'Версия для слабовидящих',
        BviPanelCloseText: 'Обычная версия сайта',
        BviFixPanel: 1,
        ver: 1.0,
        BviCloseClassAndId: '.hide-screen-fixed',
        BviTextBg: '#e53935',
        BviTextColor: '#ffffff',
        BviSizeText: 14,
        BviSizeIcon: 20,
        BviPlay: 1,
        BviPanelActive: 0,
        BviConsoleLog: 0
    }
}