const gulp = require('gulp')
const gulpLoadPlugins = require('gulp-load-plugins')
const del = require('del')
const runSequence = require('run-sequence')

const $ = gulpLoadPlugins()

// JS
gulp.task('js', () => {
    return gulp.src([
            './js/js.cookie.js',
            './js/responsivevoice.js',
            './js/bvi.js',
            './js/bvi-setting.js',
            './js/bvi-init-panel.js'
        ])
        .pipe($.stripComments())
        .pipe($.uglify({
            output: {
                max_line_len: false
            }
        }))
        .pipe($.concat('vi.js'))
        .pipe(gulp.dest('./dist/js'))
})

// CSS
gulp.task('css', () => {
    return gulp.src('./css/**/*.css')
        .pipe($.concat('vi.css'))
        .pipe($.cssmin())
        .pipe(gulp.dest('./dist/css'))
})

// Fonts
gulp.task('fonts', () => {
    return gulp.src('./fonts/**/*')
        .pipe($.flatten())
        .pipe(gulp.dest('./dist/fonts'));
});

// Clean dist
gulp.task('clean:dist', function () {
    return del.sync('./dist')
})

// Build task
gulp.task('build', function (callback) {
    runSequence('clean:dist',
        ['js', 'css', 'fonts'],
        callback
    )
})

// Watch
gulp.task('default', ['build'], () => {
    // Watchers
    gulp.watch('assets/sass/**/*.css', ['css']);
    gulp.watch('assets/js/**/*.js', ['js']);
});